<?php
//Integer is a whole value without decimal point.
$age = 23;
echo "My age is ".$age;
echo "<br>";

//Float with decimal point.
$price = 2.5;
echo "The price of kwek kwek as of today is ".$price." pesos";
echo "<br>";

//String
$name = "Gemeja Mae";
echo "My name is ".$name;
echo "<br>";

//Boolean = 1-true or 0-false
$allowed = true;
echo $allowed;
echo "<br>"; 

//Null = no value
$x = "Hello world";
$x = null;
echo $x;
$y;