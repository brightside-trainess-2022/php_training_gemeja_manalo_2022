<?php
//While loop - dont know the number of iterations.
//Do-while - loop first the do, then while.
//For loop
//Foreach loop - iterate over array variables

//While loop
$w = 1;
while ($w <= 10) {
    echo $w;
    echo "<br>";
    $w++;
}


//Do-while loop
$x = 1;

do {
    echo "Hello world!";
    $x ++;
} while ($x < 0);
    echo "<br>";


//For loop
//Intialization - Evaluted once at the beginning
//Condition - Option 1 - If its true, execute/ Option 2 - If its false, stop
//Counter - Evaluated at the end of every loop

for ($y = 1; $y <= 10; $y++) {
    echo "The number is: " . $y;
    echo "<br>";
}

//Sample exercise
//Going to the bank on the 1st of January
//Want to deposit 1000 dollars.
//Interest rate is 5%
//Withdrawl it after 5 years
$deposit = 1000;
$interest = 0.05;
for ($years = 1; $years <= 5; $years++) {
    $deposit += ($deposit * $interest);
    echo "The total amount after " . $years . " year is: " . $deposit;
    echo "<br>";
}

//Foreach loop
$names = array("Gemeja", "Mae", "Manalo");
foreach ($names as $name) {
    echo "My name is " . $name;
    echo "<br>";
}

$person = array("Name" => "Gem", 
                "Age" => 23,
                "Gender" => "Female");

foreach($person as $key => $value) {
    echo $key . ": " . $value;
    echo "<br>";
}