<?php
//Indexed array
$color1 = "Blue";
$color2 = "Red";
$color3 = "Green";
$colors = array("Blue", "Red", "Green");
echo $colors[2];
echo "<br>";

//Associative array
$kulays = array("kulay1"=>"Blue", "kulay2"=>"Red", "kulay3"=>"Red" );
var_dump($kulays);
echo "<br>";

//Multidimensional arrays
//Primary - Blue, Red, Green
//Secondary - Cyan, Magenta, Yellow
$kulays2 = array(
        "Primary"=> array("Blue", "Red", "Green"),
        "Secondary"=> array("Cyan","Magenta","Yellow")
        );
        echo $kulays2["Secondary"][0];
        echo "<br>";