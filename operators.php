<?php
// Assignment operators
//  +=  --- Add and assign
//  -=  --- Subtract and assign
//  *=  --- Multiply and assign
//  /=  --- Divide and assign
//  .=  --- Concatenate and assign
$a = 5;
$a /= 1;
echo $a += 5;
echo "<br>"; 

// Arithmetic operators are + - * / %
$b = 6;
$c = 3;
$d = $b + $c;
echo "The total is ".$d;
echo "<br>"; 

//Comparison operators
// <   --- Less than
// >   --- Greater than
// <=  --- Less than or equal to
// >=  --- Greater than or equal to
// === --- Equal to
// !== --- Not equal to
$e = 11;
$f = 11;

if ($e !== $f) {
    echo "True!";
} else {
    echo "False!";
}
echo "<br>"; 

// ++$x   --- Pre-increment
// $x++   --- Post-increment
// --$x   --- Pre-decrement
// $x--   --- Post-decrement
$g = 10;
echo ++$g;
echo "<br>"; 

//Logical operators
// And  --- Both X and Y are true
// &&   --- Both X and Y are true
// Or   --- Either X or Y are true
// ||   --- Either X or Y are true
// Xor  --- Either X or Y are true, not both
// !    --- True if X is not true
// 1 = True - 0 = False
$h = 1;
$i = 10;
 
if (!$h == $i && 1 == 1) {
    echo "True!";
} else {
    echo "False!";
}