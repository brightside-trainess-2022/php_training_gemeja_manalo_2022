<?php
//Function
//Camel case - Every word after the first one is a capital.
// ex. myFunction();

//Lower case - All lowercase, underscore inbetween.
// ex. my_function();

//Pascal case - Every word is capitalized
// ex. MyFunction();

$num = 10;

function isEven ($num) {
    if ($num % 2 == 0){
        echo $num." is an even number";
    } else{
        echo $num." is an odd number";
    }
}

isEven($num);
echo "<br>";


//Parameters and Arguments
$num1 = 5;
$num2 = 5;
//Parameters
function calculator($num1, $num2) {
    echo $num1 * $num2;
}
//Arguments
calculator($num1,$num2=7);
